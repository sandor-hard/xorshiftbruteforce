# Brute-force Xorshift configuration/tuples finder

## What is this tool for?

XorShiftBruteForce is a tool for finding Xorshift configurations/tuples for the REHARDEN Atari 8-bit demo (assembling pictures in 1024 steps in pseudo-random order).

It can be easily modified to find the configuration for your custom needs.

## What does this repo include?

This repo includes the full source code of XorShiftBruteForce in the form of an Eclipse Neon project.

There's no separate documentation written for this tool, please see the source code for examples on usage (main function).

## References

- [Xorshift on WikiPedia](https://en.wikipedia.org/wiki/Xorshift)

Sandor Teli / HARD

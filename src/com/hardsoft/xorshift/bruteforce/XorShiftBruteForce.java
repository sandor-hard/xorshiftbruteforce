/*
    Brute-force XORShift parameter/tuple finder
    used in the production of the REHARDEN demo (Atari XL/XE)

    Created by Sandor Teli / HARD (2017)

    Feel free to modify/use whatever way you like

    https://en.wikipedia.org/wiki/Xorshift
 */

package com.hardsoft.xorshift.bruteforce;

public class XorShiftBruteForce {

	final static int MAX_SHIFT = 31;
	final static int DESIRED_PERIOD = 1024;
	final static int STATE_MASK = 0x3ff;
	
	static int state;
	static int[] firstSuccParams = null;
	
	public static void main(String[] args) {
		for (int a=1;a<=MAX_SHIFT;a++) {
			for (int b=1;b<=MAX_SHIFT;b++) {
				for (int c=1;c<=MAX_SHIFT;c++) {
					int succCnt = 0;
					state = 1;
					while (succCnt<5) {
						boolean subSucc = testParams(a, b, c, false);
						if (subSucc) {
							succCnt++;
						}
						else {
							break;
						}
					}
					
					if (succCnt>1) {
						System.out.println(String.format("Success %d, %d, %d", a, b, c));
						
						if (firstSuccParams==null) {
							firstSuccParams = new int[]{a, b, c};
						}
					}
				}
			}
		}
		
		if (firstSuccParams!=null) {
			testParams(firstSuccParams[0], firstSuccParams[1], firstSuccParams[2], true);
		}
	}

	private static boolean testParams(int a, int b, int c, boolean logValues) {
		boolean succ = false;

		int distinctValueCount = 0;
		int calledCnt = 0;
		final boolean[] sawValue = new boolean[DESIRED_PERIOD];
		
		if (logValues) {
			System.out.println(String.format("Logging values for (%d, %d, %d):", a, b, c));
		}

		while (true) {
			final int r = xorshift(a, b, c);
			
			if (logValues) {
				System.out.print(String.format("%d ", r));
			}
			
			calledCnt++;
			if (r>0 && r<DESIRED_PERIOD) {
				if (sawValue[r]) {
					if (distinctValueCount==(DESIRED_PERIOD-1) && calledCnt==DESIRED_PERIOD) {
						succ = true;
					}
					else {
						//System.out.println(String.format("Repeated value at unexpected position (distinctValueCount=%d, r=%d, calledCnt=%d): %d, %d, %d", distinctValueCount, r, calledCnt, a, b, c));
					}
					break;
				}
				else {
					sawValue[r] = true;
					distinctValueCount++;
				}
			}
			else {
				//System.out.println(String.format("r is out of range (distinctValueCount=%d, r=%d, calledCnt=%d): %d, %d, %d", distinctValueCount, r, calledCnt, a, b, c));
				break;
			}
		}
		return succ;
	}

	static int xorshift(int a, int b, int c) {
	    state = (state ^ ((state << a)&STATE_MASK));
	    state = (state ^ ((state >> b)));
	    state = (state ^ ((state << c)&STATE_MASK));
	    return state;
	}	
}
